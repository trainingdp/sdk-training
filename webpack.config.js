'use strict';

const path = require('path');
const webpack = require('webpack');

// Plugins
const TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './index.ts',
  context: path.resolve(__dirname, 'src'),
  mode: "development",

  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },

  devtool: 'source-map',

  resolve: {
    extensions: [ '.js', '.json', '.ts'],
    modules: [ path.resolve(__dirname, 'node_modules') ]
  },

  plugins: [
    new TsConfigPathsPlugin({tsconfig: path.resolve(__dirname, 'tsconfig.json')}),
    new webpack.ContextReplacementPlugin(
      /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
      __dirname
    ),
    new HtmlWebpackPlugin({
      path: path.resolve(__dirname, 'dist'),
      template: './index.html',
      filename: 'index.html'
    })
  ],

  module: {
    rules: [

      /* TS Loader */
      {
        test: /\.ts$/,
        use: [
          {
            loader: 'awesome-typescript-loader',
            query: {
              configFileName: path.resolve(__dirname, 'tsconfig.json'),
              declaration: false
            }
          }
        ]
      },

      /* JSON Loader */
      {
        test: /\.json$/,
        use: ['json-loader']
      }
    ]
  },
  
  performance: {
    hints: false
  },
  
  // Dev server
  devServer: {
    hot: false,
    historyApiFallback: false,
    compress: true,
    contentBase: path.resolve(__dirname, 'dist')
  }
};
