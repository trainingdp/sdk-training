export function log(...args: any []) {
  const div = document.getElementById('logs')
  const item = typeof args[0] === 'string' ? args[0] : JSON.stringify(args[0]);
  div.innerHTML = div.innerHTML + `<div>${item}</div>`
  console.log(...args);
}
