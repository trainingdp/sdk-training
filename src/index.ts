"use strict";

import * as dapi from "@dapi/sdk";
import {Identity, Traveler, Email, Address} from "@dapi/sdk/models";
import {getPrices} from "@dapi/sdk/helpers/AirOffer";
import {getTotalCashRecord} from "@dapi/sdk/helpers/PricingRecords";
import {utils} from "@dapi/sdk-core";
import {log} from "./logger";

console.log("Welcome to the Digital API!!!");

(async () => {
    // Initialize the digital API for hosted-mode-proxy.
    // const ProxyAddress = "http://localhost:3050/V1";

    // Initialize the digital API targetting DigitalForAirlines proxy.
    const proxyAddress = "http://proxy.digitalforairlines.com/V1";

    const api = {
      airOfferApi: new dapi.AirOfferApi(proxyAddress),
      cartApi: new dapi.CartApi(proxyAddress),
      orderApi: new dapi.OrderApi(proxyAddress)
    };

    log('Starting application');
    
    // SAMPLE: 1. Count the number of offers from Nice to New York, 5 days from now. Model: utils.DateTime

    // SAMPLE: 2. Retrieve the first offer, and display the price.

    // SAMPLE: 3. Create an empty cart, and display its ID.

    // SAMPLE: 4. Add the offer we retrieved in 2) to the cart.

    // SAMPLE: 5. Add a single traveler to cart. Models: Identity, Traveler

    // SAMPLE: 6. Add and associate contacts to cart. Models: Email, Address

    // SAMPLE: 7. Retrieve cart (Do not forget last name), and display its whole content in the console.

    // SAMPLE: 8. Create an order - This will give us a record locator.
})();
