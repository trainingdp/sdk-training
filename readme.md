# Otter Playground

Easily demonstrate how to create an Order using the Digital API SDK.

## SDK Version

The playground works with @DAPI/SDK 1.7.x
It targets a proxy hosted on http://proxy.digitalforairlines.com

(Optional) You may target instead a hosted-mode-proxy running on your localhost:3050.

## Features

- @DAPI/SDK showcase
- Browser live reload
